﻿using MyPlaylists.Provider;
using MyPlaylists.Provider.Models;
using MyPlaylists.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace MyPlaylists.Services
{
    class LoadService
    {
        #region Load methods
        /// <summary>
        /// Loads Data json file from UOW
        /// </summary>
        internal static async Task<ObservableCollection<IViewModel>> LoadList()
        {
            var playlists = await GetList();
            if (!playlists.Any())
            {
                playlists = SampleList.LoadSampleList();
                SaveSamples(playlists);
            }
            return new ObservableCollection<IViewModel>(LoadViewModels(playlists));
        }

        /// <summary>
        /// Gets the Playlist
        /// </summary>
        /// <returns></returns>
        private static async Task<List<Playlist>> GetList()
        {
            List<Playlist> playlists = null;
            try
            {
                if (UOW.Playlists == null)
                    playlists = await UOW.Load();
                else
                    playlists = UOW.Playlists;
            }
            catch 
            {
                playlists = new List<Playlist>();
            }
            return playlists;
        }
        #endregion
        #region Init ViewModels Methods
        /// <summary>
        ///  Loads the PlaylistViewModels ViewModels
        /// </summary>
        /// <param name="playlists"></param>
        /// <returns></returns>
        private static System.Collections.Generic.IEnumerable<IViewModel> LoadViewModels(List<Playlist> playlists)
        {
            return (from p in playlists
                    select new PlaylistItemViewModel()
                    {
                        Playlist = p,
                        Edit = Visibility.Collapsed,
                        ItemViewModels = new ObservableCollection<IViewModel>(LoadVideoViewModels(p.Videos)),
                        HasVideos = p.Videos.Any() ? Visibility.Visible : Visibility.Collapsed,
                        NoVideos = p.Videos.Any() ? Visibility.Collapsed : Visibility.Visible,
                        TextMargin = p.Videos.Any() ? "5 5 0 0" : "25 5 0 0",
                        NoVideosText = p.Videos.Any() ? "" : string.Format("{0} has no videos.  Please click on it to add videos", p.Name),
                        InvalidNameVisible = Visibility.Collapsed
                    }).AsEnumerable();

        }
        
        /// <summary>
        /// Loads the Video ViewModel
        /// </summary>
        /// <param name="videos"></param>
        /// <returns></returns>
        private static System.Collections.Generic.IEnumerable<IViewModel> LoadVideoViewModels(List<Video> videos)
        {
            return (from v in videos
                    select new VideoViewModel()
                    {
                        DurationText = v.Duration.ToString(),
                        Video = v,
                        Edit = Windows.UI.Xaml.Visibility.Collapsed,
                        InvalidTitleVisible = Visibility.Collapsed,
                        InvalidPathVisible = Visibility.Collapsed,
                        InvalidLengthVisible = Visibility.Collapsed
                    }).AsEnumerable();

        }
        #endregion
        #region Samples Method
        /// <summary>
        /// Saves the samples if no file exists
        /// </summary>
        /// <param name="playlists"></param>
        private static void SaveSamples(List<Playlist> playlists)
        {
            UOW.Add(playlists[0]);
            UOW.Add(playlists[1]);
            UOW.SaveChanges();
        }
        #endregion
    }
}
