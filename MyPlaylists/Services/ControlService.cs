﻿using MyPlaylists.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Services
{
    class ControlService
    {
        public static ListView PlaylistListView;

        public static ObservableCollection<IViewModel> ItemViewModelCollection
        {
            get { return Services.ControlService.PlaylistListView.ItemsSource as ObservableCollection<IViewModel>; }
        }

    }
}
