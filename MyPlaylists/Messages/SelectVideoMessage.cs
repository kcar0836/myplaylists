﻿using MyPlaylists.ViewModels;
using Prism.Events;

namespace MyPlaylists.Messages
{
    public class SelectVideoMessage : PubSubEvent<VideoViewModel>
    {

    }
}
