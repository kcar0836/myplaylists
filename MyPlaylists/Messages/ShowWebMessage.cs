﻿using MyPlaylists.ViewModels;
using Prism.Events;

namespace MyPlaylists.Messages
{
    public class ShowWebMessage : PubSubEvent<IViewModel>
    {
    }
}
