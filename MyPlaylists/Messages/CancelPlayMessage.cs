﻿using MyPlaylists.ViewModels;
using Prism.Events;

namespace MyPlaylists.Messages
{
    class CancelPlayMessage : PubSubEvent<IViewModel>
    {
    }
}
