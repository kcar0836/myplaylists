﻿using MyPlaylists.ViewModels;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPlaylists.Messages
{
    class RemoveItemMessage : PubSubEvent<IViewModel>
    {
    }
}
