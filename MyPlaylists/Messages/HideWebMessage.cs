﻿using Prism.Events;

namespace MyPlaylists.Messages
{
    class HideWebMessage : PubSubEvent<object>
    {
    }
}
