﻿namespace MyPlaylists
{
    public class AppEnums
    {
        public enum Templates
        {
            MainPageData, //< !--0-- >
            Buttons, //>< !--1-- >
            PlaylistHeader, //>< !--2-- >
            PlaylistItemSelectPlay, //>< !--3-- >
            PlaylistItem, //>< !--4-- >
            PlaylistItemIsPlaying,  //>< !--5-- >
            VideoItem, //>< !--6-- >
            VideoItemSelectPlay, //>< !--7-- >
            ListViewStyle
        }
    }
}
