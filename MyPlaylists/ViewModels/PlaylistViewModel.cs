﻿using MyPlaylists.Provider.Models;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyPlaylists.Services;

namespace MyPlaylists.ViewModels
{
    public class PlaylistViewModel : BaseViewModel
    {
        #region View Properties

        private Visibility _displayButton;
        public Visibility DisplayButton
        {
            get { return _displayButton; }
            set
            {
                if (_displayButton != value)
                {
                    _displayButton = value;
                    this.OnPropertyChanged("DisplayButton");
                }
            }
        }

        private Visibility _displayPanel;
        public Visibility DisplayPanel
        {
            get { return _displayPanel; }
            set
            {
                if (_displayPanel != value)
                {
                    _displayPanel = value;
                    OnPropertyChanged("DisplayPanel");
                }
            }
        }

        private string _buttonPointer;
        public string ButtonPointer
        {
            get { return _buttonPointer; }
            set
            {
                if (_buttonPointer != value)
                {
                    _buttonPointer = value;
                    OnPropertyChanged("ButtonPointer");
                }
            }
        }

        #endregion
        #region Constructor
        /// <summary>
        /// Constructors
        /// </summary>
        public PlaylistViewModel()
        {
            DisplayPanel = Visibility.Visible;
            DisplayButton = Visibility.Collapsed;
            ItemViewModels = LoadService.LoadList().Result;

            App.AppEventAggregator.GetEvent<Messages.ShowWebMessage>().Subscribe(ShowWebView);
            App.AppEventAggregator.GetEvent<Messages.HideWebMessage>().Subscribe(HideWebView);
        }

        
        #endregion
        #region change Methods
        /// <summary>
        /// Adds new Playlist 
        /// </summary>
        public override void AddItem()
        {

            if (ItemViewModels.Where(w => w.IsNew).Count() != 1)
            {
                if (ControlService.ItemViewModelCollection.Any(a => a.IsSelected))
                {
                    var item = ChangeItemTemplate();
                    HidePreviousEdit(item);

                }
                AddItemToView();
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private async void AddItemToView()
        {
            PlaylistItemViewModel playList = new PlaylistItemViewModel() { Playlist = new Playlist(), IsNew = true, InvalidNameVisible = Visibility.Collapsed };
            ItemViewModels.Add(playList);
            await Task.Delay(1000);
            ControlService.PlaylistListView.SelectedItem = playList;

            var listViewItem = ControlService.PlaylistListView.ContainerFromItem(playList) as ListViewItem;
            ShowEdit(playList, listViewItem);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IViewModel ChangeItemTemplate()
        {
            var item = ControlService.ItemViewModelCollection.First(a => a.IsSelected);
            item.IsSelected = false;
            var row = ControlService.PlaylistListView.ContainerFromItem(item) as ListViewItem;
            row.ContentTemplate = null;
            row.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"] as DataTemplate;

            return item;
        }
        #endregion
        #region Command  Functions
        /// <summary>
        /// Hides the webview
        /// shows the Playlist
        /// </summary>
        /// <param name="obj"></param>
        private void HideWebView(object obj)
        {
            DisplayPanel = Visibility.Visible;
            DisplayButton = Visibility.Collapsed;
            ButtonPointer = "<";
        }

        
        /// <summary>
        /// Shows the Webview when play starts
        /// Hides the playlist
        /// </summary>
        /// <param name="obj"></param>
        private void ShowWebView(IViewModel obj)
        {
            DisplayPanel = Visibility.Collapsed;
            DisplayButton = Visibility.Visible;
            ButtonPointer = ">";
        }

       
        /// <summary>
        /// ListView Load from View to set videoListView
        /// </summary>
        /// <param name="sender">ListView Control</param>
        /// <param name="e"></param>
        public void PlaylistListViewLoaded(object sender, RoutedEventArgs e)
        {
            ControlService.PlaylistListView = sender as ListView;
        }

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeView(object sender, RoutedEventArgs e)
        {
            if (DisplayPanel == Visibility.Visible)
            {
                DisplayPanel = Visibility.Collapsed;
                DisplayButton = Visibility.Visible;
                ButtonPointer = ">";

            }
            else
            {
                ButtonPointer = "<";
                DisplayPanel = Visibility.Visible;
            }
        }
        #endregion
    }
}
