﻿using MyPlaylists.Provider.Models;
using MyPlaylists.Services;
using System.Linq;
using Windows.UI.Xaml;

namespace MyPlaylists.ViewModels
{
    public class VideoViewModel : BaseViewModel
    {
        #region Title Properties
        private Visibility _invalidTitleVisible;
        /// <summary>
        /// Shows or hides the InvalidNameText
        /// based on whether TitleEditTextbox is empty
        /// </summary>
        public Visibility InvalidTitleVisible
        {
            get { return _invalidTitleVisible; }
            set
            {
                _invalidTitleVisible = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Invalid Title Text message
        /// based on whether TitleEditTextbox is empty
        /// </summary>
        private string _invalidNameText;
        public string InvalidNameText
        {
            get {return _invalidNameText;}
            set
            {
                _invalidNameText = value;
                OnPropertyChanged();
            }
        }
        private Style _invalidNameStyle;
        /// <summary>
        /// Makes the TitleEditTextbox red if invalid
        /// </summary>
        public Style InvalidNameStyle
        {
            get { return _invalidNameStyle; }
            set
            {
                _invalidNameStyle = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Path Properties
        private Style _invalidPathStyle;
        /// <summary>
        /// Makes the UrlEditTextbox red if invalid
        /// </summary>
        public Style InvalidPathStyle
        {
            get { return _invalidPathStyle; }
            set
            {
                _invalidPathStyle = value;
                OnPropertyChanged();
            }
        }
        private string _invalidPathText;
        /// <summary>
        /// Invalid Title Text message
        /// based on whether UrlEditTextbox is empty
        /// </summary>
        public string InvalidPathText
        {
            get { return _invalidPathText; }
            set
            {
                _invalidPathText = value;
                OnPropertyChanged();
            }
        }
        private Visibility _invalidPathVisible;
        /// <summary>
        /// Shows or hides the UriEditValidation
        /// based on if the UrlEditTextbox is valid
        /// </summary>
        public Visibility InvalidPathVisible
        {
            get { return _invalidPathVisible; }
            set
            {
                _invalidPathVisible = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region length Duration Properties
        private string _durationText;
        /// <summary>
        /// A string of the DurationText that can
        /// be converted to the Timespan
        /// for the video length
        /// is bound to the DurationEditTextbox 
        /// Only "HH:MM:SS" are valid
        /// </summary>
        public string DurationText
        {
            get { return _durationText; }
            set
            {
                _durationText = value;
                OnPropertyChanged();
            }
        }
        private Visibility _invalidLengthVisible;
        /// <summary>
        /// Show or hides the LengthValidationText 
        /// </summary>
        public Visibility InvalidLengthVisible
        {
            get { return _invalidLengthVisible; }
            set
            {
                _invalidLengthVisible = value;
                OnPropertyChanged();
            }
        }

        private Style _invalidLengthStyle;
        /// <summary>
        /// Makes the DurationEditTextbox red if invalid
        /// </summary>
        public Style InvalidLengthStyle
        {
            get { return _invalidLengthStyle; }
            set
            {
                _invalidLengthStyle = value;
                OnPropertyChanged();
            }
        }
        private string _invalidLengthText;
        /// <summary>
        /// Invalid Title Text message
        /// based on whether DurationEditTextbox is empty
        /// </summary>
        public string InvalidLengthText
        {
            get { return _invalidLengthText; }
            set
            {
                _invalidLengthText = value;
                OnPropertyChanged();
            }
        }

        #endregion
        #region RunTest bool and Video Model Properties
        private bool _runTest;
        /// <summary>
        /// Bool is set to true if the user
        /// selects to run the test from the
        /// UrlFlyout
        /// </summary>
        public bool RunTest
        {
            get { return _runTest; }
            set
            {
                _runTest = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// A copy of the Original Video Model
        /// Is set back to the Video if the user
        /// cancels the changes
        /// </summary>
        private Video OriginalVideo;
        private Video _video;
        /// <summary>
        /// The Video Model
        /// is saved to data file
        /// where video is plays from
        /// </summary>
        public Video Video
        {
            get { return _video; }
            set
            {
                _video = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Edit Methods
        /// <summary>
        /// Get the parent playlist
        /// Calls HidePreviousEdit(playList) to hide any previous edits
        /// Calls MakeCoopy to copy to set the Video back if Edit 
        /// Set the Display panel to collapsed
        /// Set the Edit panel to visible
        /// </summary>
        public override void EditItem()
        {
            var playList = ControlService.ItemViewModelCollection.First(a => a.ItemViewModels.Any(v => v == this));
            HidePreviousEdit(playList);
            MakeCopy();
            this.Display = Windows.UI.Xaml.Visibility.Collapsed;
            this.Edit = Windows.UI.Xaml.Visibility.Visible;
            
        }
        /// <summary>
        /// Backs up the Video properties
        /// in the OriginalVideo
        /// </summary>
        private void MakeCopy()
        {
            OriginalVideo = new Video();
            OriginalVideo.Duration = Video.Duration;
            OriginalVideo.Name = Video.Name;
            OriginalVideo.Path = Video.Path;
            OriginalVideo.VideoId = Video.VideoId;
        }
        /// <summary>
        /// Shows the Display the diplay panel
        /// Hides the Edit panel
        /// Sets the Video back to the OriginalVideo
        /// which undoes the changes
        /// </summary>
        public override void CancelEdit()
        {
            Display = Visibility.Visible;
            Edit = Visibility.Collapsed;
            if (OriginalVideo != null && OriginalVideo.VideoId == Video.VideoId)
            {
                Video = OriginalVideo;
            }
            OriginalVideo = null;
        }
        #endregion
    }
}
