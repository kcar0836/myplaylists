﻿using Windows.UI.Xaml;

namespace MyPlaylists.ViewModels
{
    public class WebPlayerViewModel : BaseViewModel
    {
        #region View Properties
        private VideoViewModel _dataViewModel;
        /// <summary>
        /// The VideoViewModel that is playing
        /// the VideoViewModel.Video.Path
        /// </summary>
        public VideoViewModel DataViewModel
        {
            get { return _dataViewModel; }
            set
            {
                _dataViewModel = value;
                OnPropertyChanged();
            }
        }
        private string _urlPath;
        /// <summary>
        /// Binds the DataViewModel.Video.Path
        /// to the control
        /// Plays the DataViewModel.Video.Path
        /// </summary>
        public string UrlPath
        {
            get
            {
                return _urlPath;
            }
            set
            {
                    _urlPath = DataViewModel.Video.Path;
                    OnPropertyChanged("UrlPath");
                
            }
        }

        private Visibility _showWeb;
        /// <summary>
        /// Changes the Show or Hide of the webcontrol
        /// </summary>
        public Visibility ShowWeb
        {
            get { return _showWeb; }
            set
            {
                _showWeb = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Constuctor and Subscriber Methods
        public WebPlayerViewModel()
        {
            App.AppEventAggregator.GetEvent<Messages.CancelPlayMessage>().Subscribe(CancelPlay);
            App.AppEventAggregator.GetEvent<Messages.ShowWebMessage>().Subscribe(ShowWebView);
            
        }
        /// <summary>
        ///  Publishers set the ShowWeb to Visible
        /// </summary>
        /// <param name="obj"></param>
        private void ShowWebView(IViewModel obj)
        {
            ShowWeb = Visibility.Visible;
        }
        /// <summary>
        /// Publishers cancel the playing video
        /// </summary>
        /// <param name="obj"></param>
        private void CancelPlay(IViewModel obj)
        {
            DataViewModel = null;
            ShowWeb = Visibility.Collapsed;
            obj.IsPlaying = false;
            App.AppEventAggregator.GetEvent<Messages.HideWebMessage>().Publish(null);
        }
        #endregion
        #region View Event
        /// <summary>
        /// UpdateUrlFlyout Closed Event from the View
        /// Cancels the test play 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateUrlFlyoutClosed(object sender, object e)
        {
            CancelPlay(null);
        }
        #endregion
    }
}
