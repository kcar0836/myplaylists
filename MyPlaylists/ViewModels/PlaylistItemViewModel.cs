﻿using MyPlaylists.Provider.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyPlaylists.Services;
using System.Threading.Tasks;

namespace MyPlaylists.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class PlaylistItemViewModel : BaseViewModel
    {
        #region Name Properties

        private Visibility _invalidNameVisible;
        public Visibility InvalidNameVisible
        {
            get { return _invalidNameVisible; }
            set
            {
                _invalidNameVisible = value;
                OnPropertyChanged();
            }
        }
        private string _invalidNameText;
        public string InvalidNameText
        {
            get { return _invalidNameText; }
            set
            {
                _invalidNameText = value;
                OnPropertyChanged();
            }
        }
        private Style _invalidNameStyle;
        public Style InvalidNameStyle
        {
            get { return _invalidNameStyle; }
            set
            {
                _invalidNameStyle = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Playlst Model Properties
        private Playlist OriginalPlaylist { get; set; }
        private Playlist _playlist;
        public Playlist Playlist
        {
            get { return _playlist; }
            set
            {
                _playlist = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Text Properties
        private string _textMargin;
        public string TextMargin
        {
            get { return _textMargin; }
            set
            {
                _textMargin = value;
                OnPropertyChanged();
            }
        }

        private string _noVideosText;
        public string NoVideosText
        {
            get { return _noVideosText; }
            set
            {
                _noVideosText = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Videos Visibility Properties
        private Visibility _hasVideos;
        public Visibility HasVideos
        {
            get { return _hasVideos; }
            set
            {
                _hasVideos = value;
                OnPropertyChanged();
            }
        }
        private Visibility _noVideos;
        public Visibility NoVideos
        {
            get { return _noVideos; }
            set
            {
                _noVideos = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region VideoViewModel Properties
        private int StartIndex { get; set; }

        private VideoViewModel _playingVideo;
        public VideoViewModel PlayingVideo
        {
            get { return _playingVideo; }
            set
            {
                _playingVideo = value;
                OnPropertyChanged();
            }

        }
        private VideoViewModel _selectedVideo;
        public VideoViewModel SelectedVideo
        {
            get
            {
                return _selectedVideo;
            }
            set
            {
                _selectedVideo = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Edit Methods
        /// <summary>
        /// 
        /// </summary>
        public override void AddItem()
        {
            if (ItemViewModels == null)
                ItemViewModels = new ObservableCollection<IViewModel>();

            if (ItemViewModels.Where(w => w.IsNew).Count() != 1)
            {
                HidePreviousEdit();

                Video video = new Video();
                if (Playlist.Videos == null)
                    Playlist.Videos = new List<Video>();

                Playlist.Videos.Add(video);
                VideoViewModel item = new VideoViewModel() { Video = video, IsPlaying = false, IsNew = true, Display = Visibility.Collapsed, DurationText = "00:00:00",
                   Edit = Visibility.Visible, InvalidTitleVisible=Visibility.Collapsed,InvalidPathVisible =Visibility.Collapsed, InvalidLengthVisible=Visibility.Collapsed };
                if (ItemViewModels == null)
                    ItemViewModels = new ObservableCollection<IViewModel>();

                ItemViewModels.Add(item);
              
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void EditItem()
        {
            var playList = ControlService.ItemViewModelCollection.First(a => a == this);
            var item = ControlService.PlaylistListView.ContainerFromItem(this) as ListViewItem;
            MakeCopy();
            ShowEdit(playList, item);
            HidePreviousEdit(playList);
        }
        /// <summary>
        /// 
        /// </summary>
        private void MakeCopy()
        {
            OriginalPlaylist = new Playlist();
            OriginalPlaylist.Name = Playlist.Name;
            OriginalPlaylist.PlaylistId = Playlist.PlaylistId;
        }
        /// <summary>
        /// 
        /// </summary>
        public override void CancelEdit()
        {
            Display = Visibility.Visible;
            Edit = Visibility.Collapsed;
            if (OriginalPlaylist != null && OriginalPlaylist.PlaylistId == Playlist.PlaylistId)
            {
                Playlist.Name = OriginalPlaylist.Name;
            }
            OriginalPlaylist = null;
        }
        #endregion
        #region Play Methods
        public void PlayVideos(int start)
        {
            StartIndex = start;
            StopPrevious();
            CancelNew();
            PlayCurrent(start);
        }
        private void PlayCurrent(int start)
        {
            IsPlaying = true;
            IsSelected = false;
         
            var item = ControlService.PlaylistListView.ContainerFromItem(this) as ListViewItem;
            item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemIsPlaying]["IsPlayingShowVideos"] as DataTemplate;
            PlayVideoArray(start);

        }
        private void CancelNew()
        {
            if (ItemViewModels.Any(i => i.IsNew))
            {
                var cancelItem = ItemViewModels.First(i => i.IsNew);
                App.AppEventAggregator.GetEvent<Messages.RemoveItemMessage>().Publish(cancelItem as VideoViewModel);
            }
        }
        ///// <summary>
        ///// PlayListCommand execute
        ///// </summary>
        private async void PlayVideoArray(int start)
        {
            var iVms = ItemViewModels.ToArray();
            for(int i = start; i < iVms.Length; i++)
            {
                var item = iVms[i] as VideoViewModel;
                if (IsPlaying)
                {
                    if (start >= StartIndex)
                    {
                        PlayVideo(item);
                        await Task.Run(async () =>
                        {
                            await Task.Delay(item.Video.Duration);

                        });
                    }
                }
            }

        }

        private void PlayVideo(VideoViewModel item)
        {
           
            PlayingVideo = item;
            UpdateVideoListPlayStatus(item);
            App.AppEventAggregator.GetEvent<Messages.SelectVideoMessage>().Publish(item);
            App.AppEventAggregator.GetEvent<Messages.ShowWebMessage>().Publish(item);

        }
        private void UpdateVideoListPlayStatus(VideoViewModel item)
        {
            if (ItemViewModels.Any(a => a.IsPlaying & a != item))
            {
                var previous = ItemViewModels.First(a => a.IsPlaying & a != item);
                previous.IsPlaying = false;
            }

            item.IsPlaying = true;

        }
        #endregion
    }
}
