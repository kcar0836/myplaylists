﻿using Prism.Commands;
using MyPlaylists.Models;
using MyPlaylists.Provider;
using MyPlaylists.Provider.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.ViewModels
{
    public class VideolistViewModel : BaseViewModel
    {
        
        #region Commands
        public DelegateCommand PlayListCommand { get; set; }
        public DelegateCommand AddCommand { get; set; }
        public DelegateCommand<RelativePanel> ItemCommand { get; set; }
        public DelegateCommand<RelativePanel> SaveCommand { get; set; }
        #endregion
        #region View Properties
        #region New Video
        private int _idx;
        public int Idx
        {
            get { return _idx; }
            set
            {
                if (_idx != value)
                {
                    _idx = value;
                    OnPropertyChanged();
                }
            }
        }
        private Video _video;
        public Video Video
        {
            get { return _video; }
            set
            {
                if (_video != value)
                {
                    _video = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _durationText;
        public string DurationText
        {
            get { return _durationText; }
            set
            {
                if (_durationText != value)
                {
                    _durationText = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion
        private Visibility displayButton;
        public Visibility DisplayButton
        {
            get { return displayButton; }
            set
            {
                if (displayButton != value)
                {
                    displayButton = value;
                    this.OnPropertyChanged("DisplayButton");
                }
            }
        }
        private Visibility displayPanel;
        public Visibility DisplayPanel
        {
            get { return displayPanel; }
            set
            {
                if (displayPanel != value)
                {
                    displayPanel = value;
                    OnPropertyChanged("DisplayPanel");
                }
            }
        }

        private string _buttonPointer;
        public string ButtonPointer
        {
            get { return _buttonPointer; }
            set
            {
                if (_buttonPointer != value)
                {
                    _buttonPointer = value;
                    OnPropertyChanged("ButtonPointer");
                }
            }
        }
        private Visibility _display;
        public Visibility Display
        {
            get
            {
                return _display;
            }
            set
            {
                if (_display != value)
                {
                    _display = value;
                    OnPropertyChanged("Display");
                }
            }
        }
        private Video _selectedVideo;
        public Video SelectedVideo
        {
            get { return _selectedVideo; }
            set
            {
                if (_selectedVideo != value)
                {
                    _selectedVideo = value;
                    OnPropertyChanged("SelectedVideo");
                }
            }

        }
        private ObservableCollection<VideoModel> _videos;
        public ObservableCollection<VideoModel> Videos
        {
            get { return _videos; }
            set
            {
                if (_videos != value)
                {
                    _videos = value;
                    OnPropertyChanged("Videos");
                }
            }
        }
        #endregion
        #region Constructor
        /// <summary>
        /// Constructor 
        /// </summary>
        public VideolistViewModel()
        {
            DisplayButton = Visibility.Visible;
            DisplayPanel = Visibility.Visible;
            Display = Visibility.Collapsed;
            PlayListCommand = new DelegateCommand(PlayList);
            AddCommand = new DelegateCommand(Add);
            ItemCommand = new DelegateCommand<RelativePanel>(Edit);
            SaveCommand = new DelegateCommand<RelativePanel>(Save);
            DisplayButton = Visibility.Collapsed;
            Idx = -1;
            DurationText = "00:00:00";
            Video = new Video();
            LoadList();

        }
        /// <summary>
        /// Loads Data json file from UOW
        /// </summary>
        private async void LoadList()
        {

            var videos = await UOW.Load();

            //if (videos.Any())
            //{
            //    int count = 0;
            //    var enumerable = (from v in videos
            //                      select new VideoModel() { Display = Visibility.Collapsed,
            //                          Idx = count++,
            //                          ItemCommand = this.ItemCommand,
            //                          SaveCommand = this.SaveCommand,
            //                          Video = v,
            //                          DurationText = v.Duration.ToString()
            //             }).AsEnumerable();

            //    Videos = new ObservableCollection<VideoModel>(enumerable);
            //}
            //else
            //{
            //    this.Display = Visibility.Visible;
            //}

        }
        /// <summary>
        /// Class Name
        /// </summary>
        public override UserControl View { get; set; }


        #endregion
        #region Command  Functions
        /// <summary>
        /// SelectionChanged event fro x:bind View
        /// </summary>
        /// <param name="sender">ListView</param>
        /// <param name="e">Standard EventAgrs</param>
        public void VideoControlListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listView = sender as ListView;
            var value = listView.SelectedItem as VideoModel;
            ChangeView(null, null);
            // App.AppEventAggregator.GetEvent<VideoMessage>().Publish(value.Video);
        }
        /// <summary>
        /// PlayListCommand execute
        /// </summary>
        private async void PlayList()
        {

            foreach (var item in Videos)
            {

                videoListView.SelectedItem = item;

                //App.AppEventAggregator.GetEvent<VideoMessage>().Publish(item.Video);
                DisplayPanel = Visibility.Collapsed;
                ButtonPointer = ">";
                await Task.Run(async () =>
                {
                    await Task.Delay(item.Video.Duration);
                });

            }

        }

        /// <summary>
        /// ListViewItems
        /// </summary>
        private ListView videoListView;
        /// <summary>
        /// ListView Load from View to set videoListView
        /// </summary>
        /// <param name="sender">ListView Control</param>
        /// <param name="e"></param>
        public void VideoControlListView_Loaded(object sender, RoutedEventArgs e)
        {
            videoListView = sender as ListView;
        }

        /// <summary>
        /// Save ListViewItem edit
        /// </summary>
        private void Save(RelativePanel obj)
        {

            if ((int)obj.Tag == -1)
                AddVideoActions();
            else
                UpdateVideoActions(obj);

            UOW.SaveChanges();
            RefreshDisplay(obj);

        }

        private void AddVideoActions()
        {
             Video.Duration = ConvertDuration(DurationText);
           
            AddToVideos();
            //UOW.Add(Video);
            Display = Visibility.Collapsed;
        }
        private void AddToVideos()
        {
            if (Videos == null)
                Videos = new ObservableCollection<VideoModel>();

            Video.VideoId = Videos.Count() + 1;
            VideoModel model = new VideoModel()
            {
                ItemCommand = ItemCommand,
                SaveCommand = SaveCommand,
                Video = Video,
                Display = Visibility.Collapsed,
                DurationText = DurationText,
                Idx = Videos.Count(),

            };
            videoListView.Items.Add(model);

        }
        private void UpdateVideoActions(RelativePanel obj)
        {
            var model = videoListView.Items[(int)obj.Tag] as VideoModel;
            var item = model.Video;
            item.Duration = ConvertDuration(model.DurationText);

            //UOW.Update(item);
            RefreshDisplay(obj);
        }
        private TimeSpan ConvertDuration(string durationText)
        {
            string[] timeSplit = durationText.Split(new char[] { ':' });
            return new TimeSpan(int.Parse(timeSplit[0]), int.Parse(timeSplit[1]), int.Parse(timeSplit[2]));
        }
        /// <summary>
        /// Switches the ListViewItem from Display to Edit
        /// </summary>
        /// <param name="obj">RelativePanel from ListViewItem holds the Item Tag number index</param>
        private void Edit(RelativePanel obj)
        {
            if ((int)obj.Tag == -1)
            {
                Display = Visibility.Collapsed;
            }
            else
            {
                EditExisting(obj, GetSwitch(obj.Name));
            }
        }
        /// <summary>
        /// Switches video that already exits from Diplay to Edit
        /// or from Edit to Display
        /// </summary>
        /// <param name="obj">RelativePanel from ListViewItem holds the Item Tag number index</param>
        /// <param name="type">EditPanel or DisplayPanel</param>
        private void EditExisting(RelativePanel obj, string type)
        {
            var item = videoListView.Items[(int)obj.Tag] as VideoModel;
            obj.Visibility = Visibility.Collapsed;
            var controls = videoListView.ContainerFromItem(item) as ListViewItem;
            var control = (controls.ContentTemplateRoot as StackPanel).FindName(string.Format("{0}View", type));
            var panel = (control as UserControl).FindName(string.Format("{0}Panel", type)) as RelativePanel;
            panel.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Refreshed the Display after a save
        /// </summary>
        /// <param name="obj">RelativePanel from ListViewItem holds the Item Tag number index</param>
        private void RefreshDisplay(RelativePanel obj)
        {
            var item = videoListView.Items[(int)obj.Tag] as VideoModel;
            obj.Visibility = Visibility.Collapsed;
            var controls = videoListView.ContainerFromItem(item) as ListViewItem;
            var control = (controls.ContentTemplateRoot as StackPanel).FindName("DisplayView");
            var panel = (control as UserControl).FindName("DisplayPanel") as RelativePanel;
            var textBlock = panel.FindName("VideoTextblock") as TextBlock;
            textBlock.Text = item.Video.Name;
            panel.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Returns switch from to
        /// </summary>
        /// <param name="name">EditPanel or DisplayPanel</param>
        /// <returns>Edit or Display</returns>
        private string GetSwitch(string name)
        {
            if (name.StartsWith("Display"))
                return "Edit";
            else
                return "Display";
        }
        private void Add()
        {
            Display = Visibility.Visible;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Video = new Video();
            DurationText = "00:00:00";
            if (Display == Visibility.Collapsed)
                Display = Visibility.Visible;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeView(object sender, RoutedEventArgs e)
        {
            if (displayPanel == Visibility.Visible)
            {
                DisplayPanel = Visibility.Collapsed;
                DisplayButton = Visibility.Visible;
                ButtonPointer = ">";

            }
            else
            {
                ButtonPointer = "<";
                DisplayPanel = Visibility.Visible;
            }
        }
        #endregion

    }
}
