﻿using MyPlaylists.Services;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;

namespace MyPlaylists.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged, IViewModel
    {
        #region Property Changes Events and Methods
        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Display Edit Play bools
        /// <summary>
        /// 
        /// </summary>
        public bool IsPlaying { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsNew { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSelected
        {
            get; set;
        }
        #endregion
        #region Display Edit Visibility 
        private Visibility _edit;
        public Visibility Edit
        {
            get { return _edit; }
            set
            {
                _edit = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private Visibility _display;
        public Visibility Display
        {
            get { return _display; }
            set
            {
                _display = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region ItemViewModels
        private ObservableCollection<IViewModel> _itemViewModels;
        public virtual ObservableCollection<IViewModel> ItemViewModels
        {
            get
            {
                return _itemViewModels;
            }

            set
            {
                _itemViewModels = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Edit Methods 
        public void ShowEdit(IViewModel playList, ListViewItem item)
        {
            item.ContentTemplate = null;
            item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItem]["PlaylistItem"] as DataTemplate;

            playList.IsSelected = true;

            playList.Edit = Visibility.Visible;
            playList.Display = Visibility.Collapsed;

        }
             

        public void HidePreviousEdit(IViewModel item)
        {
            if (item.ItemViewModels.Any(a=>a.IsNew))
                item.ItemViewModels.Remove(item.ItemViewModels.First(a => a.IsNew));

            foreach (var v in item.ItemViewModels)
            {
                v.Edit = Visibility.Collapsed;
                v.Display = Visibility.Visible;
                v.IsSelected = false;
            }
        }

        public void HidePreviousEdit()
        {
            if (ControlService.ItemViewModelCollection.Any(a => a.IsNew))
                ControlService.ItemViewModelCollection.Remove(ControlService.ItemViewModelCollection.First(a => a.IsNew));

            if (ItemViewModels != null)
                foreach (var v in ItemViewModels)
                {
                    v.Display = Visibility.Visible;
                    v.Edit = Visibility.Collapsed;
                    v.IsSelected = false;
                }
        }
        #endregion
        #region virtual Edit Methods that are overriden
        public virtual void AddItem() { }

        public virtual void EditItem() { }

        public virtual void CancelEdit() { }
        #endregion
        #region Shared Play Methods
        public void StopPrevious()
        {
            if (ControlService.ItemViewModelCollection.Any(f => f.IsPlaying | f.IsSelected))
            {
                var previous = ControlService.ItemViewModelCollection.First(f => f.IsPlaying | f.IsSelected);
                var listItem = ControlService.PlaylistListView.ContainerFromItem(previous) as ListViewItem;
                previous.IsPlaying = false;
                listItem.ContentTemplate = Application.Current.Resources.MergedDictionaries
                    [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"] as DataTemplate;
            }
        }
        #endregion
    }
}