﻿using System.Collections.ObjectModel;
using Windows.UI.Xaml;

namespace MyPlaylists.ViewModels
{
    public interface IViewModel
    {
        bool IsSelected { get; set; }
        bool IsPlaying { get; set; }
        bool IsNew { get; set; }

        Visibility Display { get; set; }
        Visibility Edit { get; set; }
                
        void AddItem();
        void EditItem();
       
        void HidePreviousEdit(IViewModel item);
        void HidePreviousEdit();

        void CancelEdit();
        //void RemoveItem(IViewModel item);
        //void RemoveItem();
        ObservableCollection<IViewModel> ItemViewModels { get; set; }
     
    }
}
