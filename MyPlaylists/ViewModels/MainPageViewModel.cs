﻿using System;
using System.Linq;
using Windows.UI.Xaml;

namespace MyPlaylists.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        private IViewModel _leftSection;
        public IViewModel LeftSection
        {
            get { return _leftSection; }
            set
            {
                if (_leftSection != value)
                {
                    _leftSection = value;
                    OnPropertyChanged("LeftSection");
                }
            }
        }

        private IViewModel _mainSection;
        public IViewModel MainSection
        {
            get { return _mainSection; }
            set
            {
                if (_mainSection != value)
                {
                    _mainSection = value;
                    OnPropertyChanged("MainSection");
                }
            }
        }
        private DataTemplate _mainSectionTemplate;
        public DataTemplate MainSectionTemplate
        {
            get { return _mainSectionTemplate; }
            set
            {
                if (_mainSectionTemplate != value)
                {
                    _mainSectionTemplate = value;
                    OnPropertyChanged("MainSectionTemplate");
                }
            }
        }

        private DataTemplate _leftSectionTemplate;
        public DataTemplate LeftSectionTemplate
        {
            get { return _leftSectionTemplate; }
            set
            {
                if (_leftSectionTemplate != value)
                {
                    _leftSectionTemplate = value;
                    OnPropertyChanged("LeftSectionTemplate");
                }
            }
        }

        public MainPageViewModel()
        {
            MainSection = new WebPlayerViewModel();
            MainSectionTemplate = Application.Current.Resources.MergedDictionaries[(int)AppEnums.Templates.MainPageData]["WebPlayer"] as DataTemplate;

            App.AppEventAggregator.GetEvent<Messages.SelectVideoMessage>().Subscribe(SelectVideo);

            LeftSection = new PlaylistViewModel();
            LeftSectionTemplate = Application.Current.Resources.MergedDictionaries[(int)AppEnums.Templates.MainPageData]["Playlist"] as DataTemplate;

            App.AppEventAggregator.GetEvent<Messages.RemoveItemMessage>().Subscribe(RemoveItem);
            //Only works for a Phablet min default width is 500 yea!
            //var applicationView = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView();
            //applicationView.SetPreferredMinSize(new Windows.Foundation.Size(420, 0));

        }

        public void RemoveItem(IViewModel item)
        {
            if (item is PlaylistItemViewModel)
            {
                Services.ControlService.ItemViewModelCollection.Remove(item);

            }
            else
            {
                RemoveVideo(item);
            }
        }

        private void RemoveVideo(IViewModel item)
        {
            var parent = Services.ControlService.ItemViewModelCollection.First(i => i.ItemViewModels.Any(f=>f == item));
            parent.ItemViewModels.Remove(item);
            (parent as PlaylistItemViewModel).Playlist.Videos.Remove((item as VideoViewModel).Video);
        }
        private void SelectVideo(VideoViewModel obj)
        {
            (MainSection as WebPlayerViewModel).DataViewModel = obj;
        }

        
    }
}
