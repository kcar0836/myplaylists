﻿using System;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MyPlaylists
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPageViewModel ViewModel
        {
            get { return DataContext as MainPageViewModel; }
        }

        public MainPage()
        {
            this.InitializeComponent();
       
        }

       
       
    }
}
