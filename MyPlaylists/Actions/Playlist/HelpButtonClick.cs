﻿using Microsoft.Xaml.Interactivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Playlist
{
    class HelpButtonClick : DependencyObject, IAction
    {
        /// <summary>
        /// Is called when the Help Button in the upper right of the Header Playlist Template is click
        /// Shows the HelpFlyout in the PlaylistHeader Template
        /// </summary>
        /// <param name="sender">The Help button</param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
            var button = sender as Button;
            var flyOut = button.FindName("HelpFlyout") as Flyout;
            flyOut.ShowAt(button);
            return null;
        }
    }
}
