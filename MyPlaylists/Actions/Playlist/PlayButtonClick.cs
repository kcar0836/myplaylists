﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Playlist
{
    public class PlayButtonClick : DependencyObject, IAction
    {
        /// <summary>
        /// Calls the PlaylistItemViewModel PlayVideos method
        /// Its sends the 0 parameter to start the   PlaylistItemViewModel Videos
        /// from the first video
        /// </summary>
        /// <param name="sender">PlayButton</param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
          
            var button = sender as Button;

            PlaylistItemViewModel playlistViewModel = button.DataContext as PlaylistItemViewModel;
            playlistViewModel.PlayVideos(0);
            return null;
        }

       
    }
}
