﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Playlist
{
    /// <summary>
    /// This class changes the DataTemplates in Templates folder based on the User decision actions 
    /// </summary>
    class ItemClick : DependencyObject, IAction
    {
        #region Execute
        /// <summary>
        /// Is called when the Playlist ListView item is selected
        /// If the playlist isn't playing it switches between edit display
        /// If the playlist is playing it switches between hiding and showing
        /// the playing videos.
        /// It hides and shows previous selections.
        /// </summary>
        /// <param name="sender">The Playlist ListView</param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
            try
            {
                var listView = sender as ListView;
                var data = (parameter as ItemClickEventArgs).ClickedItem as PlaylistItemViewModel;
                var item = listView.ContainerFromItem(data) as ListViewItem;

                HidePrevious(listView, data);

                if (!data.IsPlaying)
                {
                    ShowHideSelected(listView, item, data);
                }
                else
                {
                    ShowIsPlaying(item, data);
                }
            }
            catch 
            {

            }
            return null;
        }
        #endregion
        #region ShowHide Methods
        /// <summary>
        /// It shows or hides the selected item based on the IsSelected value
        /// If false it calls ShowIsSelected
        /// If true it calls HideSelected
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="item"></param>
        /// <param name="data">Selected PlaylistItemViewModel</param>
        private void ShowHideSelected(ListView listView, ListViewItem item, PlaylistItemViewModel data)
        {
            if (!data.IsSelected)
            {
                ShowIsSelected(item, data);
            }
            else
            {
                HideSelected(listView, data);
            }
        }
        /// <summary>
        /// Shows or Hides the Playing item by changing the DataTemplate
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="data"></param>
        private void ShowHideIsPlaying(ListView listView, PlaylistItemViewModel data)
        {
            var item = ControlService.ItemViewModelCollection.First(a => a.IsPlaying);
            var row = listView.ContainerFromItem(item) as ListViewItem;
            ChangeIsPlayingTemplate(row);
        }
        #endregion
        #region Show Methods
        /// <summary>
        /// If the PlaylistItem DataTemplate isn't the ContentTemplate 
        /// its makes the ContentTemplate
        /// If it the ContentTemplate it calls ApplyItemSelectPlay
        /// </summary>
        /// <param name="item"></param>
        /// <param name="data">Selected PlaylistItemViewModel</param>
        private void ShowIsSelected(ListViewItem item, PlaylistItemViewModel data)
        {
                if (item.ContentTemplate != Application.Current.Resources.MergedDictionaries
                    [(int)AppEnums.Templates.PlaylistItem]["PlaylistItem"] as DataTemplate)
                {
                    item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                        [(int)AppEnums.Templates.PlaylistItem]["PlaylistItem"] as DataTemplate;
                    data.IsSelected = true;

                }
                else
                {
                    ApplyItemSelectPlay(item, data);
                }
            
        }
        /// <summary>
        /// If the ContentTemplate is PlaylistItemSelectPlay it changes it to IsPlayingHideVideo
        /// else it calls ChangeIsPlayingTemplate
        /// </summary>
        /// <param name="item"></param>
        /// <param name="data"></param>
        private void ShowIsPlaying(ListViewItem item, PlaylistItemViewModel data)
        {
            if (item.ContentTemplate == Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"])
            {
                item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                    [(int)AppEnums.Templates.PlaylistItemIsPlaying]["IsPlayingHideVideos"] as DataTemplate;
            }
            else
            {
                ChangeIsPlayingTemplate(item);
            }

        }
        #endregion
        #region Hide Methods
        /// <summary>
        /// Calls HideUnselected if a previous IsSelected is found that isn't the current selected
        /// Calls ShowHidesIsPlaying if a previous IsPlaying is that isn't the current
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="data"></param>
        private void HidePrevious(ListView listView, PlaylistItemViewModel data)
        {
            if (ControlService.ItemViewModelCollection.Any(a => a.IsSelected && a != data))
            {
                HideUnselected(listView, data);
            }

            if (ControlService.ItemViewModelCollection.Any(a => a.IsPlaying && a != data))
            {
                ShowHideIsPlaying(listView, data);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="item"></param>
        private void HideSelected(ListView listView, PlaylistItemViewModel item)
        {
            item.IsSelected = false;
            var row = listView.ContainerFromItem(item) as ListViewItem;
            row.ContentTemplate = null;
            row.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"] as DataTemplate;
            item.HidePreviousEdit();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listView"></param>
        /// <param name="data"></param>
        private void HideUnselected(ListView listView, PlaylistItemViewModel data)
        {
            var item = ControlService.ItemViewModelCollection.First(a => a.IsSelected && a != data) as PlaylistItemViewModel;
            HideSelected(listView, item);
        }
        #endregion
        #region IsPlaying Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="data"></param>
        private void ApplyItemSelectPlay(ListViewItem item, PlaylistItemViewModel data)
        {
            item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"] as DataTemplate;
            data.IsSelected = false;
            if (data.ItemViewModels.Any(a=> a.IsNew))
                   App.AppEventAggregator.GetEvent<Messages.RemoveItemMessage>().Publish(data.ItemViewModels.First(a => a.IsNew));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        private void ChangeIsPlayingTemplate(ListViewItem item)
        {
            if (item.ContentTemplate == Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemIsPlaying]["IsPlayingHideVideos"])
            {
                item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                    [(int)AppEnums.Templates.PlaylistItemIsPlaying]["IsPlayingShowVideos"] as DataTemplate;

            }
            else
            {
                item.ContentTemplate = Application.Current.Resources.MergedDictionaries
                    [(int)AppEnums.Templates.PlaylistItemIsPlaying]["IsPlayingHideVideos"] as DataTemplate;
            }

        }
        #endregion

    }

}