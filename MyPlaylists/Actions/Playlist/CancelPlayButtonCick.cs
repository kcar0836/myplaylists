﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Playlist
{
    class CancelPlayButtonCick : DependencyObject, IAction
    {
        /// <summary>
        /// Is called when the Cancel Play button of the PlaylistItemViewModel 
        /// of the Playlist view is selected
        /// </summary>
        /// <param name="sender">Cancel Button</param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
            var item = (sender as Button).DataContext as PlaylistItemViewModel;
            var listItem = ControlService.PlaylistListView.ContainerFromItem(item) as ListViewItem;
            item.IsPlaying = false;
            foreach(var video in item.ItemViewModels)
            {
                video.IsPlaying = false;
            }

            listItem.ContentTemplate = Application.Current.Resources.MergedDictionaries
                [(int)AppEnums.Templates.PlaylistItemSelectPlay]["PlaylistItemSelectPlay"] as DataTemplate;

            App.AppEventAggregator.GetEvent<Messages.CancelPlayMessage>().Publish(item);
            return null;
        }

    
    }
}
