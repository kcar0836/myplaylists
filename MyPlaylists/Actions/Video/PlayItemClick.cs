﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Linq;

namespace MyPlaylists.Actions.Video
{

    public class PlayItemClick : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var item = (sender as ListView).SelectedItem as VideoViewModel;
            
            if (item.IsPlaying == false)
            {
                var parent = ControlService.ItemViewModelCollection.First(i => i.ItemViewModels.Any(f => f == item)) as PlaylistItemViewModel;
                parent.PlayVideos(GetStart(item));
            }
            return null;
        }

        private int GetStart(VideoViewModel item)
        {
            var parent = ControlService.ItemViewModelCollection.First(i => i.ItemViewModels.Any(f => f == item));
            var videoVmArray = parent.ItemViewModels.ToArray();
            int start = 0;
            for (int i = 0; i < videoVmArray.Length; i++)
            {
                var videoVm = videoVmArray[i] as VideoViewModel;

                if (videoVmArray[i] == item)
                {
                    start = i;
                    break;
                }
                
            }
            return start;
        }
    }
}
