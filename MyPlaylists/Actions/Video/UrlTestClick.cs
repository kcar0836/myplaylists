﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Video
{
    class UrlTestClick : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var item = UpdatePath(sender as Button);
            item.StopPrevious();
            Publish(item);
            UpdateFlyout(sender as Button);
            return null;
        }

        private  VideoViewModel UpdatePath(Button button)
        {
            var item = button.DataContext as VideoViewModel;
            item.Video.Path = string.Format("{0}{1}", item.Video.Path.Replace("watch?v=", "embed/").Replace("&feature=player_embedded", ""), "?autoplay=1");
            item.RunTest = true;

            return item;
        }
        private void Publish(VideoViewModel item)
        {
            App.AppEventAggregator.GetEvent<Messages.SelectVideoMessage>().Publish(item);
            App.AppEventAggregator.GetEvent<Messages.ShowWebMessage>().Publish(item);
        }

        private void UpdateFlyout(Button button)
        {
            var panel = button.Parent as StackPanel;
            var flyOut = panel.FindName("UrlFlyout") as Flyout;
            flyOut.Hide();
        }
    }
}
