﻿using Microsoft.Xaml.Interactivity;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Video
{
    class UrlLostFocus : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var textBox = sender as TextBox;
           
            var panel = textBox.Parent as RelativePanel;
            if (panel != null)
            {
                var flyOut = panel.FindName("UrlFlyout") as Flyout;
                var testText = (flyOut.Content as StackPanel).FindName("UrlTestText") as TextBlock;

                if (textBox.Text.ToLower().StartsWith("https://www.youtube.com/"))
                {
                    ShowSelectTest(textBox, testText, flyOut);
                }
            }
            return null;
        }

        private void ShowSelectTest(TextBox textBox, TextBlock testText, Flyout flyOut)
        {
            if (textBox.Text.ToLower().Contains("https://www.youtube.com/watch?v="))
            {
                testText.Text = "Some Youtube providers like Vivo and UMG block fullscreen. Try it in Fullscreen mode:";
                flyOut.ShowAt(textBox);
            }
        }
    }
}
