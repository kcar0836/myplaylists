﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.Video
{
    class ItemClick : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {

            var item = (parameter as ItemClickEventArgs).ClickedItem as IViewModel;
            if (item.Display == Visibility.Visible)
               item.EditItem();
            else
               CancelEdit(item);

            return null;

        }
        private void CancelEdit(IViewModel item)
        {
            if (item.IsNew)
                App.AppEventAggregator.GetEvent<Messages.RemoveItemMessage>().Publish(item);

            item.CancelEdit();
            
        }       
    }
}