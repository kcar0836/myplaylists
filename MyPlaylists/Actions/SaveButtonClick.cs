﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Provider;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using System.Collections.Generic;
using Windows.UI.Xaml;
using System.Linq;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System;

namespace MyPlaylists.Actions
{
    public class SaveButtonClickAction : DependencyObject, IAction
    {
        #region Styles Properties
        public Style TextStyle { get { return Application.Current.Resources["InvalidTextBoxStyle"] as Style; } }
        public Style LabelStyle { get { return Application.Current.Resources["InvalidTextBlockStyle"] as Style; } }
        #endregion
        #region Execute, Update, and IsValid
        /// <summary>
        /// Executes the Save button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
            if (IsValid(sender as Button))
            {
                UOW.Playlists = new List<Provider.Models.Playlist>();

                foreach (var item in ControlService.ItemViewModelCollection)
                {
                    UpdatePlaylist(item);
                }

                UOW.SaveChanges();
                App.AppEventAggregator.GetEvent<Messages.UpdateMessage>().Publish(null);
            }
            return null;
        }
        /// <summary>
        /// Adds new playlist
        /// </summary>
        /// <param name="item"></param>
        private void UpdatePlaylist(IViewModel item)
        {
            var playListItemViewModel = item as PlaylistItemViewModel;
            var playList = playListItemViewModel.Playlist;

            if (playList.Videos == null)
                playList.Videos = new List<Provider.Models.Video>();

            UOW.Add(playList);
            item.IsNew = false;
        }
        /// <summary>
        /// Starts Validation
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        private bool IsValid(Button button)
        {
            IViewModel data = button.DataContext as IViewModel;
            if (data is PlaylistItemViewModel)
                return ValidatePlaylistItem(data);
            else
                return ValidateVideo(data);

        }
        #endregion
        #region Validate Video Methods
        /// <summary>
        /// Validates Video
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool ValidateVideo(IViewModel data)
        {
            bool result = true;
            var videoViewModel = data as VideoViewModel;

            result = ValidTitle(videoViewModel);
            result = ValidPath(videoViewModel);
            result = ValidDuration(videoViewModel);

            return result;
           
        }

        /// <summary>
        /// Validates videoDuration
        /// </summary>
        /// <param name="videoViewModel"></param>
        /// <returns></returns>
        private bool ValidDuration(VideoViewModel videoViewModel)
        {
            bool result = true;
            Duration duration = new Duration(new System.TimeSpan());
            System.TimeSpan timeSpan = new System.TimeSpan();

            bool validDuration = System.TimeSpan.TryParse(videoViewModel.DurationText, out timeSpan);
            if (videoViewModel.DurationText == "00:00:00" | !validDuration)
                result = UpdateInvalidLength(videoViewModel);
            else
                videoViewModel.Video.Duration = timeSpan;
            return result;

        }
        /// <summary>
        /// Sends invalid video duration message
        /// </summary>
        /// <param name="videoViewModel"></param>
        /// <returns></returns>
        private bool UpdateInvalidLength(VideoViewModel videoViewModel)
        {
            videoViewModel.InvalidLengthStyle = TextStyle;
            videoViewModel.InvalidLengthText = "Duration must be in HH:MM:SS format. It must greater then 0";
            videoViewModel.InvalidLengthVisible = Visibility.Visible;
            return false;
        }
        /// <summary>
        /// Validates video path
        /// </summary>
        /// <param name="videoViewModel"></param>
        /// <returns></returns>
        private bool ValidPath(VideoViewModel videoViewModel)
        {
            bool result = true;
            if (videoViewModel.Video.Path == null)
                videoViewModel.Video.Path = "";
            
            if (!videoViewModel.Video.Path.ToLower().StartsWith("https//www.yahoo.com"))
            {
                videoViewModel.InvalidPathStyle = TextStyle;
                videoViewModel.InvalidPathText = "Url must be a youtube video.";
                videoViewModel.InvalidPathVisible = Visibility.Visible;

                result = false;
            }
            return result;
        }
        /// <summary>
        /// Validates video title
        /// </summary>
        /// <param name="videoViewModel"></param>
        /// <returns></returns>
        private bool ValidTitle(VideoViewModel videoViewModel)
        {
            bool result = true;
            if (string.IsNullOrEmpty(videoViewModel.Video.Name))
            {
                videoViewModel.InvalidNameStyle = TextStyle;
                videoViewModel.InvalidNameText = "Name cannot be empty";
                videoViewModel.InvalidTitleVisible = Visibility.Visible;
                result = false;
            }

            return result;
        }
        #endregion
        #region Validate Playlist Method
        private bool ValidatePlaylistItem(IViewModel data)
        {
            bool result = true;
            var playListItemViewModel = data as PlaylistItemViewModel;
            if (string.IsNullOrEmpty(playListItemViewModel.Playlist.Name))
            {
                playListItemViewModel.InvalidNameStyle = TextStyle;
                playListItemViewModel.InvalidNameText = "Name cannot be empty";
                playListItemViewModel.InvalidNameVisible = Visibility.Visible;
                result = false;
            } 
            return result;
        }
        #endregion
    }
}
