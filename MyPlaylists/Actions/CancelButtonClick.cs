﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions
{
    class CancelButtonClick : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var data = (sender as Button).DataContext as IViewModel;
            if (data.IsNew)
            {
                //Remove item from ItemViewModels parent by going to the MainPageViewModel
                App.AppEventAggregator.GetEvent<Messages.RemoveItemMessage>().Publish(data);
            }
            else
            {
                data.CancelEdit();
            }
            return null;
        }
        
        
    }
}
