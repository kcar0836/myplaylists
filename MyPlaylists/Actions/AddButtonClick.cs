﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions
{
    class AddButtonClick : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var item =(sender as Button).DataContext as IViewModel;
            item.AddItem();
            
            return null;
        }

    }
  
}
