﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.Provider;
using MyPlaylists.Services;
using MyPlaylists.ViewModels;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Linq;
namespace MyPlaylists.Actions
{
    class DeleteButtonClick : DependencyObject, IAction
    {
        #region Module Field
        IViewModel parent;
        #endregion
        #region Execute and Save
        /// <summary>
        /// Execute DeleteButtonClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object Execute(object sender, object parameter)
        {
            var button = (sender as Button);
            var item = button.DataContext as IViewModel;
            RemoveItem(item);
            Save(item);

            return null;
        }
        /// <summary>
        /// Remove IViewModel
        /// </summary>
        /// <param name="item"></param>
        private void RemoveItem(IViewModel item)
        {
            if (item is PlaylistItemViewModel)
            {
                Services.ControlService.ItemViewModelCollection.Remove(item);
            }
            else
            {
                GetDeletedVideoParent(item);
            }
        }
        /// <summary>
        /// Saves to Deleted record
        /// </summary>
        /// <param name="uow"></param>
        private void Save(IViewModel uow)
        {
            UOW.Playlists = new List<Provider.Models.Playlist>();
            Provider.Models.Playlist deleteList = null;

            if (uow is PlaylistItemViewModel)
                deleteList = (uow as PlaylistItemViewModel).Playlist;
            
            foreach (var item in ControlService.ItemViewModelCollection)
            {
                UpdatePlaylistModel(uow, item, deleteList);
            }

            UOW.SaveChanges();
            App.AppEventAggregator.GetEvent<Messages.UpdateMessage>().Publish(null);
        }
        #endregion
        #region Playlists Updates
        /// <summary>
        /// Removes the videoform the playlist
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="item"></param>
        /// <param name="deleteList"></param>
        private void UpdatePlaylistModel(IViewModel uow, IViewModel item, Provider.Models.Playlist deleteList)
        {
            var playList = (item as PlaylistItemViewModel).Playlist;

            if (item == parent)
            {
                var video = (uow as VideoViewModel).Video;
                playList.Videos.Remove(video);
            }

            if (playList != deleteList)
                UOW.Add(playList);
        }

        /// <summary>
        /// Gets the Deleted PlaylistViewModel of the VideoViewModel
        /// </summary>
        /// <param name="item"></param>
        private void GetDeletedVideoParent(IViewModel item)
        {
            parent = ControlService.ItemViewModelCollection.First(i => i.ItemViewModels.Any(f => f == item));
        }
        #endregion
    }
}