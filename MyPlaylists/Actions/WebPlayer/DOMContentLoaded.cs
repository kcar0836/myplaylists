﻿using Microsoft.Xaml.Interactivity;
using MyPlaylists.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyPlaylists.Actions.WebPlayer
{
    class DOMContentLoaded : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            var view = sender as WebView;
            var item = view.DataContext as WebPlayerViewModel;

            if (item.DataViewModel != null && item.DataViewModel.RunTest)
            {
                RunTest(view, item);
                item.DataViewModel.RunTest = false;
                item.DataViewModel.IsPlaying = true;
            }
            return null;
        }
        private void RunTest(WebView view, WebPlayerViewModel item)
        {

            var html = GetHtml(view.Source.ToString());
            if (html.Result.Contains("An error occurred."))
                UpdateBlockedVideo(view, item);
            else
                UpdateFlyout(view, item, "Fullscreen succeeded.  The url has been updates.");

        }
#pragma warning disable
        public static async Task<string> GetHtml(string url)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                return client.GetStringAsync(url).Result;
            }
        }
        
        private void UpdateBlockedVideo(WebView view, WebPlayerViewModel item)
        {
            item.DataViewModel.Video.Path = string.Format("{0}", item.DataViewModel.Video.Path.Replace("embed/", "watch?v=").Replace("&feature=player_embedded", "").Replace("?autoplay=1", ""));
            string message = "The Youtube Provider has blocked Fullscreen load.  The Url will load as a standard screen.";
            UpdateFlyout(view, item, message);
        }

        private void UpdateFlyout(WebView view, WebPlayerViewModel item, string message)
        {
            var flyOut = view.FindName("UpdateUrlFlyout") as Flyout;

            var text = (flyOut.Content as StackPanel).Children[0] as TextBlock;
            text.Text = message;
            flyOut.ShowAt(view);
            App.AppEventAggregator.GetEvent<Messages.SelectVideoMessage>().Publish(item.DataViewModel);
            App.AppEventAggregator.GetEvent<Messages.ShowWebMessage>().Publish(item.DataViewModel);

        }

    }
}
