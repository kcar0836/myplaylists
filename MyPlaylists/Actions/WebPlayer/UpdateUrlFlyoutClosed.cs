﻿using Microsoft.Xaml.Interactivity;
using Windows.UI.Xaml;

namespace MyPlaylists.Actions.WebPlayer
{
    class UpdateUrlFlyoutClosed : DependencyObject, IAction
    {
        public object Execute(object sender, object parameter)
        {
            App.AppEventAggregator.GetEvent<Messages.CancelPlayMessage>().Publish(null);
            return null;
        }
    }
}
