﻿using MyPlaylists.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MyPlaylists.Views
{
    public sealed partial class PlaylistView : UserControl
    {
        /// <summary>
        /// The ViewModel
        /// Used for the new UWP x:Bind to put
        /// the event to the ViewModel 
        /// </summary>
        public PlaylistViewModel ViewModel
        {
            get
            {
                return this.DataContext as PlaylistViewModel;
            }
        }


        public PlaylistView()
        {
            this.InitializeComponent();

        }


    }
}