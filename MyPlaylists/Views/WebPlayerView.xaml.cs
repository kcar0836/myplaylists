﻿using MyPlaylists.ViewModels;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MyPlaylists.Views
{
    public sealed partial class WebPlayerView : UserControl
    {
        /// <summary>
        /// The ViewModel
        /// Used for the new UWP x:Bind to put
        /// the event to the ViewModel 
        /// </summary>
        public WebPlayerViewModel ViewModel
        {
            get
            {
                return this.DataContext as WebPlayerViewModel;
            }
        }


        public WebPlayerView()
        {
            this.InitializeComponent();
        }

      
    }
}
