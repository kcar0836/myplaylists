﻿using MyPlaylists.Provider.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Storage;

namespace MyPlaylists.Provider
{
    public class UOW
    {
        /// <summary>
        /// Lit of UOW items
        /// </summary>
        public static List<Playlist> Playlists { get; set; }
        /// <summary>
        /// Gets the data file
        /// </summary>
        /// <param name="fileName">the data file name</param>
        /// <returns>Returns the Storage File</returns>
        public static async Task<StorageFile> GetVideoFile(string fileName)
        {
            StorageFolder localfolder = ApplicationData.Current.LocalFolder;
            StorageFile file = null;
            if (!(await localfolder.TryGetItemAsync(fileName) == null))
                file = await localfolder.GetFileAsync(fileName);

            return file;
        }
        /// <summary>
        /// Loads the data into the Vidoes list
        /// </summary>
        /// <returns>returns the data Video List</returns>
        public static async Task<List<Playlist>> Load()
        {
            Playlists = new List<Playlist>();
            StorageFile file = await GetJsonFile("MyPlaylistData.json");
            if (file != null)
            {
                using (var read = await file.OpenAsync(FileAccessMode.ReadWrite))
                {
                    using (var stream = read.AsStream())
                    {
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(Playlists.GetType());
                        Playlists = ser.ReadObject(stream) as List<Playlist>;
                        stream.Dispose();
                    }
                    read.Dispose();
                }
            }
            return Playlists;
        }

        /// <summary>
        /// Inserts new Video object into data file
        /// </summary>
        /// <param name="video">Video file to be added</param>
        public static void Add(Playlist playlist)
        {
            if (Playlists == null)
                Playlists = new List<Playlist>();

            if (Playlists.Any())
                playlist.PlaylistId = Playlists.Max(i => i.PlaylistId) + 1;
            else
                playlist.PlaylistId = 1;

            Playlists.Add(playlist);
        }
        public static void Add()
        {
            if (Playlists == null)
                Playlists = new List<Playlist>();

            var playlist = new Playlist();
            playlist.Name = "";
            playlist.PlaylistId = 0;
            playlist.Videos = new List<Video>();
            Playlists.Add(playlist);
        }
        /// <summary>
        /// Updates an existing video into the data file 
        /// </summary>
        /// <param name="video"></param>
        public static void Update(Playlist playlist)
        {
            if (Playlists == null)
                Playlists = new List<Playlist>();

            if (playlist.PlaylistId == 0)
                Add(playlist);
            else
                MakeChanges(playlist);
        }
        /// <summary>
        /// Removes video from data list
        /// </summary>
        /// <param name="video">Video will be removed</param>
        public static void Delete(Playlist playlist)
        {
            if (Playlists == null)
                Playlists = new List<Playlist>();

            if (Playlists.Where(a => a.PlaylistId == playlist.PlaylistId).Count() > 0)
                Playlists.Remove(Playlists.Where(a => a.PlaylistId == playlist.PlaylistId).First());
        }
        /// <summary>
        /// Make changes to existing item 
        /// </summary>
        /// <param name="video">video item to update</param>
        private static void MakeChanges(Playlist playlist)
        {
            var updateItem = Playlists.Where(a => a.PlaylistId == playlist.PlaylistId).First();
            updateItem.Name = playlist.Name;
            updateItem.Videos = playlist.Videos;
            
        }
        /// <summary>
        /// Submits the changes
        /// </summary>
        public static async void SaveChanges()
        {
            if (Playlists != null)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Playlist>));
                using (MemoryStream memoryStream = new MemoryStream())
                {

                    serializer.WriteObject(memoryStream, Playlists);
                    bool success = await CreateFile("MyPlaylistData.json", memoryStream);

                }
            }
        }
        /// <summary>
        /// Gets the Json data file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Returns the Storeage file</returns>
        private static async Task<StorageFile> GetJsonFile(string fileName)
        {
            StorageFolder localfolder = ApplicationData.Current.LocalFolder;
            StorageFile file = null;
            if (await localfolder.TryGetItemAsync(fileName) != null)
            {
                file = await localfolder.GetFileAsync(fileName);
            }
            return file;
        }
        //public static async Task<bool> SaveFile(string fileName, MemoryStream dataStream)
        //{
        //}

        /// <summary>
        /// Creates the data file if it doesn't exist
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataStream"></param>
        /// <returns>the Storage File</returns>
        private static async Task<bool> CreateFile(string fileName, MemoryStream dataStream)
        {

            try
            {

                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

                using (Stream fileStream = await file.OpenStreamForWriteAsync())
                {
                    dataStream.Seek(0, SeekOrigin.Begin);
                    dataStream.CopyTo(fileStream);
                    fileStream.Flush();
                    fileStream.Dispose();
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
