﻿using System.Collections.Generic;

namespace MyPlaylists.Provider.Models
{
    public class Playlist
    {
        public int PlaylistId { get; set; }
        public string Name { get; set; }
        public List<Video> Videos { get; set; }
    }
}
