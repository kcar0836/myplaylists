﻿using System;

namespace MyPlaylists.Provider.Models
{
    public class Video
    {
        public int VideoId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
