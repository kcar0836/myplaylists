﻿using System;
using System.Collections.Generic;
using MyPlaylists.Provider.Models;
namespace MyPlaylists.Provider
{
    public class SampleList
    {

        public static List<Playlist> LoadSampleList()
        {
            return new List<Playlist>() { new Playlist { Name = "Sample Playlist", PlaylistId = 1, Videos = LoadSampleVideos() }, new Playlist { PlaylistId=2, Name="Sample Playlist 2", Videos=LoadSampleVideos2() } };
        }

        private static List<Video> LoadSampleVideos()
        {
            Video video1 = new Video() { VideoId = 1, Name = "Fleetwood Mac Rhiannon 1977", Path = "https://www.youtube.com/embed/IT1q7L4QA0A?autoplay=1", Duration = new TimeSpan(0, 6, 44) };
            Video video2 = new Video() { VideoId = 2, Name = "Maria Mckee Lone Justice Shelter", Path = "https://www.youtube.com/embed/2648xZNiNB4?autoplay=1", Duration = new TimeSpan(0, 4, 33) };
            Video video3 = new Video() { VideoId = 3, Name = "Fleetwood Mac Go Your Own Way 1997", Path= "https://www.youtube.com/embed/07NQZXMSNDk?autoplay=1", Duration = new TimeSpan(0, 5, 35) };
            Video video4 = new Video() { VideoId = 4, Name = "Undisputed Truth Smiling Faces", Path= "https://www.youtube.com/watch?v=BUmNud_3lcQ", Duration = new TimeSpan(0, 7, 11) };
            return new List<Video>() { video1, video2, video3, video4 };
        }

        private static List<Video> LoadSampleVideos2()
        {
            Video video1 = new Video() { VideoId = 1, Name = "The Corrs Breathless", Path = "https://www.youtube.com/embed/vzerbXFwGCE?autoplay=1", Duration = new TimeSpan(0, 3, 42) };
            Video video2 = new Video() { VideoId = 2, Name = "U2 One 1997", Path = "https://www.youtube.com/embed/1VuuSnzO_t0?autoplay=1", Duration = new TimeSpan(0, 5, 10) };
            Video video3 = new Video() { VideoId = 3, Name = "U2 With or Without You 1987", Path = "https://www.youtube.com/embed/WZKnQRkWzLI?autoplay=1", Duration = new TimeSpan(0, 5, 11) };
            return new List<Video>() { video1, video2, video3 };
        }

       

    }


}
